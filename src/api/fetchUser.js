export function fetchUser() {
    let users = []

    return fetch("https://randomuser.me/api?seed=tw&results=20").then((response) =>{
        return response.json();
    }).then((data) => {{
        users = data.results.map((item) => {
            return {
                uuid: item.login.uuid,
                name: `${item.name.first} ${item.name.last}`,
                email: item.email,
                thumbnail: item.picture.thumbnail
            }
        })

        return users;
    }})
}