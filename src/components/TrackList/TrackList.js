import React from "react";
import "./TrackList.css";
import Track from "../Track/Track";

const TrackList = (props) => {
    return (
        <div className={"TrackList"}>
            {props?.data?.map((item,index) => {
                return (
                    <Track item={item} key={index} onAdd={props.onAdd}/>
                )
            })}

        </div>
    );
}

export default TrackList;