import React, {useState} from "react";
import "./SearchBar.css";

const SearchBar = (props) => {

    const handleChangeSearch = (e) =>{
        props.onSearch(e.target.value);
    }

    return (
        <div className={"SearchBar"}>
            <input placeholder={"Enter A Song Title"} onChange={handleChangeSearch}/>
        </div>
    );
}

export default SearchBar;