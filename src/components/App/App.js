import React, {useEffect} from "react";
import "./App.css";
import SearchBar from "../SearchBar/SearchBar";
import SearchResults from "../SearchResults/SearchResults";
import Playlist from "../Playlist/Playlist";
import {mockData} from "../../api/mockData";
import {fetchUser} from "../../api/fetchUser";

const App = () => {
    const [searchResults, setSearchResults] = React.useState([]);
    const [search, setSearch] = React.useState("");
    const [list, setList] = React.useState([]);
    const [users, setUsers] = React.useState([]);

    const handleSearch =(e)=> {
        setSearch(e);
    }

    useEffect(() => {
        fetchUser().then((data) => {
            setUsers(data)
        });
    },[])

    useEffect(() => {
      const result = users.filter((item) => {
          return item.name.toLowerCase().includes(search.toLowerCase());
      });

        setSearchResults(result);
    }, [search, users]);

    const handleAdd = (item) => {
        console.log('item', item);
        setList([...list, item]);
    }

    const handleRemove = (id) => {
        const newList = list.filter((item, index) => item.uuid !== id);
        setList(newList);
    }


return (
    <div>
      <h1>
        Ja<span className="highlight">mmm</span>ing
      </h1>
        <div className="App">
            <SearchBar onSearch={handleSearch} />
            <div className={"App-playlist"}>
                <SearchResults data={searchResults} onAdd={handleAdd} />
                <Playlist data={list} onRemove={handleRemove}/>
            </div>
        </div>
    </div>
  );
}

export default App;