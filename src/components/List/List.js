import React from "react";
import "./List.css";

const List = (props) => {
    const {item,index} = props;

    const handleAdd = () => {
        props.onAdd(item);
    }

    const handleRemove = (id) => {
        props.onRemove(id);
    }

    return (
        <div className={"Track"}>
            <div className={"Track-Image"}>
                <img src={item?.thumbnail} alt={item?.name} />
            </div>
            <div className={"Track-Information"} style={{marginLeft:"20px"}}>
                <h3>{item?.name}</h3>
                <p>{item?.email}</p>
            </div>
            <button className="Track-action" onClick={()=> handleRemove(item?.uuid)}>
                -
            </button>
        </div>
    );
}

export default List;