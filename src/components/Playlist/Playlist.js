import React from "react";
import "./Playlist.css";
import List from "../List/List";

const Playlist = (props) => {
    return (
        <div className={"Playlist"}>
            <input defaultValue={"New list"}/>
            {props?.data.map((it) => {
                return (
                    <List item={it} key={it.id} onRemove={props.onRemove}/>
                )
            })}
            <button className={"Playlist-save"} >SAVE TO SPOTIFY</button>
        </div>
    );
}

export default Playlist;