import React from "react";
import "./Track.css";

const Track = (props) => {
    const {item,index} = props;

    const handleAdd = () => {
        props.onAdd(item);
    }

    const renderAction = () => {
        if (props?.isRemoval) {
            return (
                <button className="Track-action">
                    -
                </button>
            );
        }
        return (
            <button className="Track-action" onClick={()=> handleAdd()}>
                +
            </button>
        );
    }

    return (
        <div className={"Track"} key={item?.id}>
            <div className={"Track-Image"}>
                <img src={item?.thumbnail} alt={item?.name} />
            </div>
            <div className={"Track-Information"} style={{marginLeft:"20px"}}>
                <h3>{item?.name}</h3>
                <p>{item?.email}</p>
            </div>
            {renderAction()}
        </div>
    );
}

export default Track;